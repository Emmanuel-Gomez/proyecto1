<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }

    public function list()
    {
        return view('ecommerceProduct');
    }

    public function product()
    {
        return view('ecommerceProductSingle');
    }  

    public function checkout()
    {
        return view ('ecommerceProductCheck');
    }
    
    public function finance()
    {
        return view('dashboardFinance');
    }

    public function sales()
    {
        return view('dashboardSales');
    }

    public function influencer()
    {
        return view ('dashboardInfluencer');
    } 
    
    public function finder()
    {
        return view('influencerFinder');
    }

    public function profile()
    {
        return view ('influencerProfile');
    }
}
