<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class formController extends Controller
{
    public function index()
    {
        return view('elements');
    }

    public function Validation()
    {
        return view('validation');
    }

    public function multiselect()
    {
        return view('multiselect');
    }

    public function picker()
    {
        return view('datepicker');
    }

    public function bootstrap()
    {
        return view('bootstrap');
    }
}
