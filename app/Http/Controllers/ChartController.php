<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function index()
    {
        return view('chart-c3');
    }

    public function chartist()
    {
        return view('chartist');
    }

    public function chart()
    {
        return view('charts');
    }

    public function morris()
    {
        return view('morris');
    }

    public function sparkline()
    {
        return view('sparkline');
    }

    public function gauge()
    {
        return view('gauge');
    }
}
