<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UiElementController extends Controller
{
    public function index()
    {
        return view('cards');
    }

    public function general()
    {
        return view('general');
    }

    public function carousel()
    {
        return view('carousel');
    }

    public function listGroup()
    {
        return view('listgroup');
    }

    public function typography()
    {
        return view('typography');
    }

    public function according()
    {
        return view('accordions');
    }

    public function tabs()
    {
        return view('tabs');
    }
}
