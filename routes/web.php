<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Estas son las rutas de la pestaña Dashboard */
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::get('/ProductList', [App\Http\Controllers\DashboardController::class, 'list'])->name('ProductList');

Route::get('/ProductSingle', [App\Http\Controllers\DashboardController::class, 'product'])->name('ProductSingle');

Route::get('productCheck', [App\Http\Controllers\DashboardController::class, 'checkout'])->name('productCheck');

Route::get('/finance', [App\Http\Controllers\DashboardController::class, 'finance'])->name('finance');

Route::get('/sales', [App\Http\Controllers\DashboardController::class, 'sales'])->name('sales');

Route::get('/influencer', [App\Http\Controllers\DashboardController::class, 'influencer'])->name('influencer');

Route::get('/finder', [App\Http\Controllers\DashboardController::class, 'finder'])->name('finder');

Route::get('profile', [App\Http\Controllers\DashboardController::class, 'profile'])->name('profile');

/*Estas son las rutas de la pestaña UI Elements */

Route::get('/cards', [App\Http\Controllers\UiElementController::class, 'index'])->name('cards');

Route::get('/general', [App\Http\Controllers\UiElementController::class, 'general'])->name('general');

Route::get('/carousel', [App\Http\Controllers\UiElementController::class, 'carousel'])->name('carousel');

Route::get('/listgroup', [App\Http\Controllers\UiElementController::class, 'listGroup'])->name('listgroup');

Route::get('/typography', [App\Http\Controllers\UiElementController::class, 'typography'])->name('typography');

Route::get('/accordions', [App\Http\Controllers\UiElementController::class, 'according'])->name('accordions');

Route::get('/tabs', [App\Http\Controllers\UiElementController::class, 'tabs'])->name('tabs');

/*Rutas para la seccion Charts */

Route::get('/C3', [App\Http\Controllers\ChartController::class, 'index'])->name('C3');

Route::get('/chartist', [App\Http\Controllers\ChartController::class, 'chartist'])->name('chartist');

Route::get('/chart', [App\Http\Controllers\ChartController::class, 'chart'])->name('chart');

Route::get('/Morris', [App\Http\Controllers\ChartController::class, 'morris'])->name('Morris');

Route::get('/Sparkline', [App\Http\Controllers\ChartController::class, 'sparkline'])->name('Sparkline');

Route::get('/Gauge', [App\Http\Controllers\ChartController::class, 'gauge'])->name('Gauge');

/*Rutas para la pestaña Forms */

Route::get('/elements', [App\Http\Controllers\FormController::class, 'index'])->name('chartist');

Route::get('/validation', [App\Http\Controllers\FormController::class, 'Validation'])->name('validation');

Route::get('/multiselect', [App\Http\Controllers\FormController::class, 'multiselect'])->name('multiselect');

Route::get('/picker', [App\Http\Controllers\FormController::class, 'picker'])->name('picker');

Route::get('/bootstrap', [App\Http\Controllers\FormController::class, 'bootstrap'])->name('bootstrap');

/*Rutas para la pestaña de Tables */

Route::get('/general-tables', [App\Http\Controllers\TablesController::class, 'index'])->name('general-tables');

Route::get('/dataTables', [App\Http\Controllers\TablesController::class, 'data'])->name('dataTables');
